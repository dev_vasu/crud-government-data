import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import AppBar from './components/AppBar';
import UpperTab from './components/UpperTab';

class App extends Component {
    render() {
        return (
            <div>
                <AppBar />
                <UpperTab />
            </div>
        );
    }
}

export default App;
