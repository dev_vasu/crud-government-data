import React from 'react';
import Button from 'material-ui/Button';

const Demo = () => (
    <Button variant="raised" color="primary">
        Hello World
    </Button>
);

export default Demo;