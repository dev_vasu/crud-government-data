var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var DrivingSchema   = new Schema({
    name: String,
    dob: String,
    address: String,
    number: String,
});

module.exports = mongoose.model('Driving', DrivingSchema);
