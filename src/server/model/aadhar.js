var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var AaddharSchema   = new Schema({
    name: String,
    dob: String,
    address: String,
    number: String
});

module.exports = mongoose.model('Aadhar', AaddharSchema);
