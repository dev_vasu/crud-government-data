import express from 'express';
import mongoose from 'mongoose';
import path from 'path';
import bodyParser from 'body-parser';
import Aadhar from './model/aadhar';
import Driving from './model/driving';
import Passport from './model/passport';

mongoose.connect('mongodb://localhost:27017/MyDb'); // connect to our database

const app = express();

const clientDirectory = './target/client';
const clientBuildDirectory = './target/client/build';
// =============================================================================
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// =============================================================================
app.use('/static', express.static(path.join(clientBuildDirectory, 'static')));
app.use(express.static(path.join(clientDirectory, 'build')));
// =============================================================================
var router = express.Router();              // get an instance of the express Router
// =============================================================================
router.use(function(req, res, next) {
    console.log('Something is happening.');
    next(); 
});
// =============================================================================
router.route('/aadharCards')
    .post(function(req, res) {
        let aadhar = new Aadhar();      // create a new instance of the Aadhar model
        aadhar.name = req.body.name; 
        aadhar.dob = req.body.dob; 
        aadhar.address = req.body.address; 
        aadhar.number = req.body.number; 
        aadhar.save(function(err) {
            if (err)
                res.send(err);
            res.json({ message: 'aadhar created!' });
        });
    })
    .get(function(req, res) {
        Aadhar.find(function(err, aadhars) {
            if (err)
                res.send(err);
            res.json(aadhars);
        });
    });

// =============================================================================
router.route('/aadharCards/:aadhar_id')
    .get(function(req, res) {
        Aadhar.find({"number":req.params.aadhar_id}, function(err, aadhar) {
            if (err)
                res.send(err);
            res.json(aadhar);
        });
    });
//=============================================================================
router.route('/passports')
    .post(function(req, res) {
        let passport = new Passport();      // create a new instance of the Aadhar model
        passport.name = req.body.name; 
        passport.dob = req.body.dob; 
        passport.address = req.body.address; 
        passport.number = req.body.number; 
        passport.save(function(err) {
            if (err)
                res.send(err);
            res.json({ message: 'passport created!' });
        });
    })
    .get(function(req, res) {
        Passport.find(function(err, passports) {
            if (err)
                res.send(err);
            res.json(passports);
        });
    });
// =============================================================================
router.route('/passports/:passport_id')
    .get(function(req, res) {
        Passport.find({"number":req.params.passport_id}, function(err, passport) {
            if (err)
                res.send(err);
            res.json(passport);
        });
    });
// =============================================================================
router.route('/drivinglicenses')
    .post(function(req, res) {
        let driving = new Driving();      // create a new instance of the Aadhar model
        driving.name = req.body.name; 
        driving.dob = req.body.dob; 
        driving.address = req.body.address; 
        driving.number = req.body.number; 
        driving.save(function(err) {
            if (err)
                res.send(err);
            res.json({ message: 'driving license created!' });
        });
    })
    .get(function(req, res) {
        Driving.find(function(err, drivinglicenses) {
            if (err)
                res.send(err);
            res.json(drivinglicenses);
        });
    });

// =============================================================================
router.route('/drivinglicenses/:driving_id')
    .get(function(req, res) {
        Driving.find({"number":req.params.driving_id}, function(err, driving) {
            if (err)
                res.send(err);
            res.json(driving);
        });
    });
// =============================================================================

app.use('/api', router);

app.listen(3000, () => {
  console.log('listening on 3000');
});
